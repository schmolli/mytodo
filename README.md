# mytodo-msa

An example implementation of a msa running of the base architecture https://gitlab.reutlingen-university.de/poegel/microservice-architecture

## Current State of Implementation
* Service mytodo manages todo items consisting of an ID and a description.
* Items can be added or retrieves (all or by a certain ID)
* IDs are set at the server side.
* Items are stored in a slice (main memory - no data base yet in use).
* Path-Prefix ist /mytodo, each service method extends this to a full path.
* The service is exposed on port 10000.

## Dockerfile/CI/CD
* Image is tagged with schmolli/mytodo and uploaded to DockerHub.
* Current version is available on dockerhub: https://hub.docker.com/repository/docker/schmolli/mytodo/general.
* No pipline to build and push the image automatically.

## Current State of the K8S Configuration
* Everybody can read all items or one item with a certain ID.
* Only authorized users can add new items.

## Issues/Findings
* Why does service respond with 501 if we try to add an item without authentication?
* Why do we not use https?
* Service is accessible from windows using the IP of wsl (wsl hostname -I).
* Inside wsl we can use localhost.