package handler

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.reutlingen-university.de/schmolli/todo/data"
	"gitlab.reutlingen-university.de/schmolli/todo/model"
)

func AddTodo(c *gin.Context) {
	var todo model.Todo
	if err := c.BindJSON(&todo); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
	} else {
		todo.ID = data.NextId
		data.NextId++
		data.Todos = append(data.Todos, todo)
		c.IndentedJSON(http.StatusCreated, todo)
	}
}

func GetTodos(c *gin.Context) {
	c.IndentedJSON(http.StatusOK, data.Todos)
}

func GetToDoByID(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	for _, t := range data.Todos {
		if t.ID == id {
			c.IndentedJSON(http.StatusOK, t)
			return
		}
	}
	c.AbortWithError(http.StatusNotFound, fmt.Errorf("todo with ID %d not found", id))
}

func DeleteToDoByID(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	var todo *model.Todo = nil
	if todo, err = data.DeleteToDoWithId(id); err != nil {
		c.AbortWithError(http.StatusNotFound, err)
	}

	c.IndentedJSON(http.StatusOK, *todo)
}

func DeleteAllTodos(c *gin.Context) {
	len := len(data.Todos)
	data.DeleteAll()
	c.IndentedJSON(http.StatusOK, fmt.Sprintf("Deleted %d todo items", len))
}
