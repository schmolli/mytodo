package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.reutlingen-university.de/schmolli/todo/handler"
)

func main() {
	r := gin.Default()
	r.GET("/mytodo/health", handler.Health)
	r.GET("/mytodo/todos", handler.GetTodos)
	r.GET("/mytodo/todos/:id", handler.GetToDoByID)
	r.POST("/mytodo/todos", handler.AddTodo)
	r.DELETE("/mytodo/todos/:id", handler.DeleteToDoByID)
	r.DELETE("/mytodo/todos/clear", handler.DeleteAllTodos)
	r.Run(":10000")
}
