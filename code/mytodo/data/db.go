package data

import (
	"fmt"

	"gitlab.reutlingen-university.de/schmolli/todo/model"
)

var Todos = []model.Todo{}
var NextId = 0

func DeleteToDoWithId(id int) (todo *model.Todo, err error) {
	delIndex := -1
	for pos, t := range Todos {
		if t.ID == id {
			delIndex = pos
			break
		}
	}
	if delIndex == -1 {
		return nil, fmt.Errorf("todo with ID %d not found", id)
	} else {
		todo := Todos[delIndex]
		deleteIndex(delIndex)
		return &todo, nil
	}
}

func DeleteAll() {
	//Create empty slice
	Todos = make([]model.Todo, 0)
}

func deleteIndex(pos int) {
	Todos = append(Todos[:pos], Todos[pos+1:]...)
}
